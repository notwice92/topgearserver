<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $email = $_POST['email'];
    $pw = $_POST['pw'];
    
    $result = $db->login($email, $pw);
    if($result != false){
        echo json_encode($result);
    }else{
        echo "error";
    }
    ?>