<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $user_id = $_POST['user_id'];
    $model_id = $_POST['model_id'];
    $favor = $_POST['favor'];
    
    $result = $db->favorite($user_id, $model_id, $favor);
    if($result != false){
        echo "success";
    }else{
        echo "error";
    }
    ?>