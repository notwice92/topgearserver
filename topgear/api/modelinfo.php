<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $user_id = $_POST['user_id'];
    $model_id = $_POST['model_id'];
    
    $result = $db->modelinfo($user_id, $model_id);
    if($result != false){
        echo json_encode($result);
    }else{
        echo "error";
    }
    ?>