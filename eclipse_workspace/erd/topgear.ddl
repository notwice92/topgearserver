DROP TABLE IF EXISTS used_car_image;
DROP TABLE IF EXISTS car_model_image;
DROP TABLE IF EXISTS saved_model;
DROP TABLE IF EXISTS user_log;
DROP TABLE IF EXISTS user_info;
DROP TABLE IF EXISTS used_car;
DROP TABLE IF EXISTS car_model_info;
DROP TABLE IF EXISTS manufacturer;

/**********************************/
/* Table Name: manufacturer */
/**********************************/
CREATE TABLE manufacturer(
		manufacturer_id               		MEDIUMINT(10)		 NULL  COMMENT 'manufacturer_id',
		country                       		MEDIUMINT(10)		 NULL  COMMENT 'country'
) COMMENT='manufacturer';

/**********************************/
/* Table Name: car_model_info */
/**********************************/
CREATE TABLE car_model_info(
		model_id                      		MEDIUMINT(10)		 NULL  COMMENT 'model_id',
		name                          		MEDIUMINT(10)		 NULL  COMMENT 'name',
		manufacturer_id               		MEDIUMINT(10)		 NULL  COMMENT 'manufacturer_id'
) COMMENT='car_model_info';

/**********************************/
/* Table Name: used_car */
/**********************************/
CREATE TABLE used_car(
		product_id                    		MEDIUMINT(10)		 NULL  COMMENT 'product_id',
		price                         		MEDIUMINT(10)		 NULL  COMMENT 'price',
		year                          		MEDIUMINT(10)		 NULL  COMMENT 'year',
		km                            		MEDIUMINT(10)		 NULL  COMMENT 'km',
		model_id                      		MEDIUMINT(10)		 NULL  COMMENT 'model_id',
		reg_date                      		MEDIUMINT(10)		 NULL  COMMENT 'reg_date',
		dissmiss_date                 		MEDIUMINT(10)		 NULL  COMMENT 'dissmiss_date',
		origion_data                  		MEDIUMINT(10)		 NULL  COMMENT 'origin_data'
) COMMENT='used_car';

/**********************************/
/* Table Name: user_info */
/**********************************/
CREATE TABLE user_info(
		user_id                       		MEDIUMINT(10)		 NULL  COMMENT 'user_id',
		passwd                        		MEDIUMINT(10)		 NULL  COMMENT 'passwd',
		name                          		MEDIUMINT(10)		 NULL  COMMENT 'name',
		reg_date                      		MEDIUMINT(10)		 NULL  COMMENT 'reg_date',
		reave_date                    		MEDIUMINT(10)		 NULL  COMMENT 'reave_date'
) COMMENT='user_info';

/**********************************/
/* Table Name: user_log */
/**********************************/
CREATE TABLE user_log(
		user_id                       		MEDIUMINT(10)		 NULL  COMMENT 'user_id',
		reg_date                      		MEDIUMINT(10)		 NULL  COMMENT 'reg_date'
) COMMENT='user_log';

/**********************************/
/* Table Name: saved_model */
/**********************************/
CREATE TABLE saved_model(
		user_id                       		MEDIUMINT(10)		 NULL  COMMENT 'user_id',
		model_id                      		MEDIUMINT(10)		 NULL  COMMENT 'model_id'
) COMMENT='saved_model';

/**********************************/
/* Table Name: car_model_image */
/**********************************/
CREATE TABLE car_model_image(
		image_id                      		MEDIUMINT(10)		 NULL  COMMENT 'image_id',
		image_url                     		MEDIUMINT(10)		 NULL  COMMENT 'image_url',
		model_id                      		MEDIUMINT(10)		 NULL  COMMENT 'model_id',
		position                      		MEDIUMINT(10)		 NULL  COMMENT 'position'
) COMMENT='car_model_image';

/**********************************/
/* Table Name: used_car_image */
/**********************************/
CREATE TABLE used_car_image(
		image_id                      		MEDIUMINT(10)		 NULL  COMMENT 'image_id',
		position                      		MEDIUMINT(10)		 NULL  COMMENT 'position',
		image_url                     		MEDIUMINT(10)		 NULL  COMMENT 'image_url',
		product_id                    		MEDIUMINT(10)		 NULL  COMMENT 'product_id'
) COMMENT='used_car_image';


ALTER TABLE manufacturer ADD CONSTRAINT IDX_manufacturer_PK PRIMARY KEY (manufacturer_id);

ALTER TABLE car_model_info ADD CONSTRAINT IDX_car_model_info_PK PRIMARY KEY (model_id);
ALTER TABLE car_model_info ADD CONSTRAINT IDX_car_model_info_FK0 FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (manufacturer_id);

ALTER TABLE used_car ADD CONSTRAINT IDX_used_car_PK PRIMARY KEY (product_id);
ALTER TABLE used_car ADD CONSTRAINT IDX_used_car_FK0 FOREIGN KEY (model_id) REFERENCES car_model_info (model_id);

ALTER TABLE user_info ADD CONSTRAINT IDX_user_info_PK PRIMARY KEY (user_id);

ALTER TABLE user_log ADD CONSTRAINT IDX_user_log_FK0 FOREIGN KEY (user_id) REFERENCES user_info (user_id);

ALTER TABLE saved_model ADD CONSTRAINT IDX_saved_model_FK0 FOREIGN KEY (user_id) REFERENCES user_info (user_id);
ALTER TABLE saved_model ADD CONSTRAINT IDX_saved_model_FK1 FOREIGN KEY (model_id) REFERENCES car_model_info (model_id);

ALTER TABLE car_model_image ADD CONSTRAINT IDX_car_model_image_PK PRIMARY KEY (image_id);
ALTER TABLE car_model_image ADD CONSTRAINT IDX_car_model_image_FK0 FOREIGN KEY (model_id) REFERENCES car_model_info (model_id);

ALTER TABLE used_car_image ADD CONSTRAINT IDX_used_car_image_PK PRIMARY KEY (image_id);
ALTER TABLE used_car_image ADD CONSTRAINT IDX_used_car_image_FK0 FOREIGN KEY (product_id) REFERENCES used_car (product_id);

