#!/usr/local/bin/node
var phantom = require('phantom');
var cheerio = require('cheerio');
var request = require('request');
var iconv = require('iconv-lite')

iconv.skipDecodeWarning = true;

function get_brand()
{
	console.log('브랜드 가져오기')
	var brandUrl = 'http://www.encar.com/common/combo/modelgroupfordb.json?method=carList';
	var brandForm = {
		'mnfccd' : '001',
		'_' : null
	}

	request.post({
		uri: brandUrl,
		encoding: 'binary',
		form : brandForm,
	}, function(err,httpResponse,body){
		var data = iconv.decode(body, 'euckr');
		var json = JSON.parse(data);

		console.log('--------------------------------------------');
		console.log('브랜드 번호 => ' + brandForm.mnfccd)
		console.log('차량 번호 => ' + json[0].abbreviation);
		console.log('차량 이름 => ' + json[0].name);
		console.log('--------------------------------------------');

		get_model(brandForm.mnfccd, json[0].abbreviation);
		// for(var i in json)
		// {
		// 	console.log('차량번호 => ' + json[i].abbreviation);
		// 	console.log('차량이름 => ' + json[i].name);
		// 	console.log('--------------------------------------------');
		// }
	})
}

function get_model(brandNumber, modelNumber)
{
	console.log('모델 가져오기.');
	var modelUrl = 'http://www.encar.com/common/combo/cardbmodel.json?method=carList';
	var modelForm = {
		'mnfccd':brandNumber,
		'mdlgroupcd':modelNumber
	}

	request.post({
		uri: modelUrl,
		encoding: 'binary',
		form : modelForm,
	}, function(err,httpResponse,body){
		var data = iconv.decode(body, 'euckr');
		var json = JSON.parse(data);
		if(json.length > 1)
		{
			console.log('세부 모델있음..');
			console.log('차량 세부번호 => ' + json[0].abbreviation);
			console.log('차량 이름 => ' + json[0].label);

		}
		else
		{
			console.log('단일 차량....');
			console.log('차량 세부번호 => ' + json[0].abbreviation);
			console.log('차량 이름 => ' + json[0].label);
		}
		console.log('--------------------------------------------');

		get_year(brandNumber, json[0].type, json[0].abbreviation)
		// for(var i in json)
		// {
		// 	console.log('차량 세부번호 => ' + json[i].abbreviation);
		// 	console.log('차량 이름 => ' + json[i].label);
		// 	console.log('--------------------------------------------');
		// }
	})
}


function get_year(brandNumber, modelType, modelNumber)
{
	console.log('연식 가져오기.')
	//세부 모델 가져오기.
	var yearUrl = 'http://www.encar.com/common/combo/jatoYr.json?method=carList';
	var yearForm = {
		'mnfccd':modelType,
		'mdlcd':modelNumber,
		'valueType':'name',
		'_':null
	}

	request.post({
		uri: yearUrl,
		encoding: 'binary',
		form : yearForm,
	}, function(err,httpResponse,body){
		var data = iconv.decode(body, 'euckr');
		var json = JSON.parse(data);

		for(var i in json)
		{
			console.log('연식 => ' + json[i].abbreviation);
		}
		console.log('--------------------------------------------');

		get_price(brandNumber, modelNumber, json[0].abbreviation)
	})
}

function get_price(brandNumber, subNumber, year)
{
	var priceUrl = 'http://www.encar.com/db/db_carsinfo.do?method=newprice&mnfccd='+brandNumber+'&mdlcd='+subNumber+'&caryear='+year;

	phantom.create()
    .then(instance => {
        phInstance = instance;
        return instance.createPage();
    })
    .then(page => {
        sitepage = page;
        return page.open(priceUrl);
    })
    .then(status => {
        //console.log('시작 => '+status);
        return sitepage.property('content');
    })
    .then(content => {
        //console.log(content);
        var $ = cheerio.load(content);

        $('tbody .new tr').each(function (e) {
        	if(e == 0)
        	{
        		console.log('구분 1 => ' + $('.grade', this).text());
	        	console.log('구분 2 => ' + $('.detailgrd', this).text());
	        	console.log('미션 => ' + $('.mission', this).text());
	        	console.log('가격 => ' + $('.price', this).text().replace(/[^0-9]/g,''));
	        	console.log('기본사양 품목 => ' + $('.item .item', this).text().trim());
	        	console.log('선택사양 품목 => ' + $('.option', this).text().trim());
	        	console.log('---------------------------------------------');
        	}
        })
	 	sitepage.close();
	 	phInstance.exit();
        //console.log('종료???');
    })
    .catch(error => {
        //console.log(error);
        phInstance.exit();
    });
}
//http://www.encar.com/db/db_carsinfo.do?method=newprice&mnfccd=브랜드번호&mdlcd=세부모델번호&caryear=연식

get_brand();