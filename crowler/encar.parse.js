#!/usr/local/bin/node

var siteUrl = 'http://m.encar.com/ca/carsearch.do#/%7B%22type%22%3A%22car%22%2C%22action%22%3A%22%22%2C%22title%22%3A%22%EA%B5%AD%EC%82%B0%EC%B0%A8%C2%B7%EC%88%98%EC%9E%85%EC%B0%A8%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22view%22%3A%22viewList%22%2C%22sort%22%3A%22ModifiedDate%22%7D';

var phantom = require('phantom');
var cheerio = require('cheerio');
var request = require('request');

var Db = require('mysql-activerecord');
var db = new Db.Adapter({
	server: 'localhost',
	username: 'root',
	password: '1q2w3e4r1!',
	database: 'topgear',
	reconnectTimeout: false
});

var sitepage = null;
var phInstance = null;

phantom.create()
.then(instance => {
	phInstance = instance;
	return instance.createPage();
})
.then(page => {
	sitepage = page;
	return page.open(siteUrl);
})
.then(status => {
	return sitepage.property('content');
})
.then(content => {
        console.log(content);
        var j_data = get_car_data(content); //parsing
        db.insert('parsed', j_data, function(err, info) { //db insert
        	if(err) console.log(err);
        	phInstance.exit();
        	process.exit();
        });
	 	sitepage.close();
	 	phInstance.exit();
    })
.catch(error => {
        console.log(error);
        phInstance.exit();
    });

function get_car_data (content) {
	var $ = cheerio.load(content);

	all_data = [];

	$('.info').each(function (e) {
		var parent = $(this).parent('a').attr('href').replace(/[^0-9]/g,'');
		var brand = $('.title .cls strong', this).text();
		var product = $('.title .cls em', this).text();
		var sub_name = $('.title .dtl strong', this).text();
		var trim = $('.title .dtl em', this).text();
		var mission = $('.option1 .trs', this).text();
		var fuel = $('.option1 .fue', this).text();
		var location = $('.option1 .loc', this).text();
		var used = $('.option1 .year', this).text();
		var km = $('.option1 .km', this).text();
		var price = $('.option2 .price', this).text();

		var json_data = {
			pr_number : parent * -1,
			pr_brand : brand,
			pr_product : product,
			pr_model : sub_name,
			pr_trim : trim == '' ? '-': trim,
			pr_mission : mission,
			pr_fuel : fuel,
			pr_location : location,
			pr_date : used,
			pr_mileage : km.replace(/[^0-9]/g,''),
			pr_price : price.replace(/[^0-9]/g,'')
		}



		if(used.match(/(.*?)\/(.*?)식\((.*?)년형\)/g) != null)
		{
			var re = /(.*?)\/(.*?)식\((.*?)년형\)/g;
			var str = used;
			var m;

			while ((m = re.exec(str)) !== null) {
				if (m.index === re.lastIndex) {
					re.lastIndex++;
				}
			    // View your result using the m-variable.
				//console.log(m);
				json_data['pr_year'] = m[1];
				json_data['pr_month'] = m[2];
				json_data['pr_respec'] = 1;
				json_data['pr_respec_year'] = m[3];
			}


		}
		else
		{
			var re = /(.*?)\/(.*?)식/g;
			var str = used;
			var m;

			while ((m = re.exec(str)) !== null) {
				if (m.index === re.lastIndex) {
					re.lastIndex++;
				}
				json_data['pr_year'] = m[1];
				json_data['pr_month'] = m[2];
				json_data['pr_respec'] = 0;
				json_data['pr_respec_year'] = 0;
			}

		}

		json_data['pr_created'] = Math.round(+new Date()/1000);

		all_data.push(json_data);

	});
	//console.log(all_data);
	return all_data;


}